import QtQuick 2.4
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.2
import QtWebKit 3.0

import ModuleName 1.0
import KeyEmiterModule 1.0

import QtQuick.Window 2.0

import QtQuick.Controls.Styles 1.4

Item{

    property variant globalForJs: 10
    property variant firstInit: true
    property variant forJs: "   MMM HELLO WORLD!"


    property variant click_pos_x: 0
    property variant click_pos_y: 0


    property int defaultWidth: 480
    property int defaultHeight: 800

    width: Screen.width
    height: Screen.height

    property double measure: Math.min(Math.min(width, height) / defaultWidth, Math.max(width, height) / defaultHeight)
    property double textScale: Math.sqrt( measure)
    visible: true
//    visibility: Window.FullScreen
    objectName: "rect"


    function calculateDirection(direction){
       switch (direction)
       {
       case "Up":
           click_pos_x=webview.width/2;
           click_pos_y=webview.height/4;;
           break
       case "Bottom":
           click_pos_x=webview.width/2;
           click_pos_y=(webview.height*75)/100;
           break
       case "Left":
           click_pos_x=(webview.width*25)/100;
           click_pos_y=webview.height/2;
           break
       case "Right":
           click_pos_x=(webview.width*75)/100;
           click_pos_y=webview.height/2;
           break
       case "Up_Right":
           click_pos_x=(webview.width*75)/100;
           click_pos_y=(webview.height*25)/100;
           break
       case "Right_Bottom":
           click_pos_x=(webview.width*75)/100;
           click_pos_y=(webview.height*75)/100;
           break
       case "Bottom_Left":
           click_pos_x=(webview.width*25)/100;
           click_pos_y=(webview.height*75)/100;
           break
       case "Left_Up":
           click_pos_x=(webview.width*25)/100;
           click_pos_y=(webview.height*25)/100;
           break
       case "Center":
           click_pos_x=(webview.width)/2;
           click_pos_y=(webview.height)/2;
           break
       }

    }


    TypeName
    {
        id:myObj
        someText_: "some TEXT"
        onSomeSignal_:
        {
            Qt.quit()
        }
    }




    MessageDialog
    {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption)
        {

            messageDialog.text = caption+forJs+myObj.someText_;
            messageDialog.open();

        }


    }

    KeyEmiterType
    {

        id:myKeyEmit
//        anchors.fill: parent

//        width:400
//        height: 200
    }



    WebView
    {

//                anchors.fill: mousearea1
//            anchors.fill: parent
            width: (parent.width*99)/100
            height: (parent.height*99)/100





            id: webview
            url: "http://agar.io"




            onNavigationRequested:
            {
                var schemaRE = /^\w+:/;
                if (schemaRE.test(request.url))
                {
                    request.action = WebView.AcceptRequest;
                }
                else
                {
                    request.action = WebView.IgnoreRequest;
                    // delegate request.url here
                }

            }

//                Button
//                {
//                            id: button1
//                            text: qsTr("Press Me 1")
//    //
//                            onClicked: messageDialog.show("HHX")/*,myObj.mySlot()*/
//    //                            onClicked: messageDialog.show("HHX")

//                }

            Button{

                id:keyEmit_W
                text: "SHOT"
                onClicked: myKeyEmit.keyEmit_("87")

                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.rightMargin: 100
                anchors.bottomMargin: 21


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 50
                            implicitHeight: 50
                            border.width: control.activeFocus ? 2 : 1
                            border.color: "#1EB7D5"
                            radius: 30
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                            }
                        }
                    }


            }

            Button{
                id:keyEmit_Space
                text: "SEP"
                onClicked: myKeyEmit.keyEmit_("32")

//                width: 60
//                height: 60


                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.rightMargin: 20
                anchors.bottomMargin: 21



                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 70
                            implicitHeight: 70
                            border.width: control.activeFocus ? 2 : 1
                            border.color: "#1EB7D5"
                            radius: 40
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                            }
                        }
                    }

            }


            Rectangle{

                implicitWidth: 120
                implicitHeight: 120

                radius: 60

                color:"#00FFFFFF" //transparent


                border.color: "#1EB7D5"


                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.leftMargin: 20
                anchors.bottomMargin: 20






                Button{

                    id:mouseEmit_Up

                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 44
                    anchors.bottomMargin: 85

                    onClicked: calculateDirection("Up"),myKeyEmit.mouseDirectionEmit_("Up",click_pos_x,click_pos_y)



                    text: mouseEmit_Up.pressed ? "/\\" : "  "



                    style: ButtonStyle {

                            background: Rectangle {

                                implicitWidth: 30
                                implicitHeight: 30

                                border.width: control.activeFocus ? 2 : 1
                                border.color: control.pressed ? "#1EB7D5" : "#00FFFFFF"

                                radius: 40

                                color:control.pressed ? "#eee" : "#00FFFFFF" //transparent

                            }
                        }



                }



                Button{

                    id:mouseEmit_Down                    
                    text: mouseEmit_Down.pressed ? "\\/" : "  "
                    onClicked: calculateDirection("Bottom"),myKeyEmit.mouseDirectionEmit_("Bottom",click_pos_x,click_pos_y)

                    width: 30
                    height: 30

                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 44
                    anchors.bottomMargin: 5

                    style: ButtonStyle {

                            background: Rectangle {

                                implicitWidth: 30
                                implicitHeight: 30
                                border.width: control.activeFocus ? 2 : 1
                                border.color: control.pressed ? "#1EB7D5" : "#00FFFFFF"

                                radius: 40

                                color:control.pressed ? "#eee" : "#00FFFFFF" //transparent
                            }
                        }


                }

                Button{

                    id:mouseEmit_Left
                    text: mouseEmit_Left.pressed ? "<" : "  "
                    onClicked: calculateDirection("Left"),myKeyEmit.mouseDirectionEmit_("Left",click_pos_x,click_pos_y)

                    width: 30
                    height: 30

                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 4
                    anchors.bottomMargin: 44

                    style: ButtonStyle {

                            background: Rectangle {

                                implicitWidth: 30
                                implicitHeight: 30
                                border.width: control.activeFocus ? 2 : 1
                                border.color: control.pressed ? "#1EB7D5" : "#00FFFFFF"

                                radius: 40

                                color:control.pressed ? "#eee" : "#00FFFFFF" //transparent
                            }
                        }


                }

                Button{

                    id:mouseEmit_Right
                    text: mouseEmit_Right.pressed ? ">" : "  "
                    onClicked: calculateDirection("Right"),myKeyEmit.mouseDirectionEmit_("Right",click_pos_x,click_pos_y)

                    width: 30
                    height: 30

                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 85
                    anchors.bottomMargin: 44

                    style: ButtonStyle {

                            background: Rectangle {

                                implicitWidth: 30
                                implicitHeight: 30
                                border.width: control.activeFocus ? 2 : 1
                                border.color: control.pressed ? "#1EB7D5" : "#00FFFFFF"

                                radius: 40

                                color:control.pressed ? "#eee" : "#00FFFFFF" //transparent
                            }
                        }
                }

                Button{

                    id:mouseEmit_Up_Right
                    text: mouseEmit_Up_Right.pressed ? "/" : "  "
                    onClicked: calculateDirection("Up_Right"),myKeyEmit.mouseDirectionEmit_("Up_Right",click_pos_x,click_pos_y)

                    width: 23
                    height: 23

                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 80
                    anchors.bottomMargin: 79

                    style: ButtonStyle {

                            background: Rectangle {

                                implicitWidth: 30
                                implicitHeight: 30
                                border.width: control.activeFocus ? 2 : 1
                                border.color: control.pressed ? "#1EB7D5" : "#00FFFFFF"

                                radius: 40

                                color:control.pressed ? "#eee" : "#00FFFFFF" //transparent
                            }
                        }

                }

                Button{

                    id:mouseEmit_Right_Bottom
                    text: mouseEmit_Right_Bottom.pressed ? "\\" : "  "
                    onClicked: calculateDirection("Right_Bottom"),myKeyEmit.mouseDirectionEmit_("Right_Bottom",click_pos_x,click_pos_y)

                    width: 23
                    height: 23

                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 80
                    anchors.bottomMargin: 17

                    style: ButtonStyle {

                            background: Rectangle {

                                implicitWidth: 30
                                implicitHeight: 30
                                border.width: control.activeFocus ? 2 : 1
                                border.color: control.pressed ? "#1EB7D5" : "#00FFFFFF"

                                radius: 40

                                color:control.pressed ? "#eee" : "#00FFFFFF" //transparent
                            }
                        }

                }

                Button{

                    id:mouseEmit_Bottom_Left
                    text: mouseEmit_Bottom_Left.pressed ? "/" : "  "
                    onClicked: calculateDirection("Bottom_Left"),myKeyEmit.mouseDirectionEmit_("Bottom_Left",click_pos_x,click_pos_y)

                    width: 23
                    height: 23

                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 17
                    anchors.bottomMargin: 17

                    style: ButtonStyle {

                            background: Rectangle {

                                implicitWidth: 30
                                implicitHeight: 30
                                border.width: control.activeFocus ? 2 : 1
                                border.color: control.pressed ? "#1EB7D5" : "#00FFFFFF"

                                radius: 40

                                color:control.pressed ? "#eee" : "#00FFFFFF" //transparent
                            }
                        }

                }

                Button{

                    id:mouseEmit_Left_Up
                    text: mouseEmit_Left_Up.pressed ? "\\" : "  "
                    onClicked: calculateDirection("Left_Up"),myKeyEmit.mouseDirectionEmit_("Left_Up",click_pos_x,click_pos_y)

                    width: 23
                    height: 23

                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 17
                    anchors.bottomMargin: 78

                    style: ButtonStyle {

                            background: Rectangle {

                                implicitWidth: 30
                                implicitHeight: 30
                                border.width: control.activeFocus ? 2 : 1
                                border.color: control.pressed ? "#1EB7D5" : "#00FFFFFF"

                                radius: 40

                                color:control.pressed ? "#eee" : "#00FFFFFF" //transparent
                            }
                        }

                }



                Button{

                    id:mouseEmit_Center

                    onClicked: calculateDirection("Center"),myKeyEmit.mouseDirectionEmit_("Center",click_pos_x,click_pos_y)

                    width: 23
                    height: 23

                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 48
                    anchors.bottomMargin: 47

                    style: ButtonStyle {

                            background: Rectangle {

                                implicitWidth: 30
                                implicitHeight: 30
                                border.width: control.activeFocus ? 2 : 1
                                border.color: control.pressed ? "#00FFFFFF" : "#1EB7D5"

                                radius: 40

                                color:control.pressed ? "#eee" : "#00FFFFFF" //transparent
                            }
                        }

                }



            }






            Keys.onPressed:
            {
                if (event.key == Qt.Key_A) {
                            console.log("move Up"+"--"+globalForJs++);
                            event.accepted = true;
                            myKeyEmit.mouseDirectionEmit_("Up");

                        }

            }



//                MouseArea {
//                       id: mousearea1
//                       anchors.fill: parent

//                       propagateComposedEvents: true

//                       acceptedButtons: Qt.LeftButton | Qt.RightButton
//                       onClicked:
//                       {
//                           console.log("click at " + mouse.x + ", " + mouse.y+",  btn : "+mouse.button);
//                           mouse.accepted = false;
//                       }

//                       onPressed: mouse.accepted = false;
//                       onReleased: mouse.accepted = false;
//                       onDoubleClicked: mouse.accepted = false;
//                       onPositionChanged: mouse.accepted = false;
//                       onPressAndHold: mouse.accepted = false;
//               }



       }






}

