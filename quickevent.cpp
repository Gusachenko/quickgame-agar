#include "quickevent.h"

#include <QQuickView>
#include <QDebug>
#include <QApplication>

QQuickView * view;


QuickEvent::QuickEvent()
{
    setAcceptedMouseButtons(Qt::AllButtons);

}

void QuickEvent::showQuick()
{
    view = new QQuickView();

    view->setSource(QUrl(QStringLiteral("qrc:/main.qml")));
////    view->resize(500,300);
    view->show();
}

void QuickEvent::mousePressEvent(QMouseEvent *event)
{
    QQuickItem::mousePressEvent(event);
    qDebug() << event->pos();
}

void QuickEvent::keyEmit_(QString keyName)
{
    float x=80;
    float y=80;

        if ("87"==keyName)// 87  - "W"
        {

            QKeyEvent keyPressEvent_W(QEvent::KeyPress, Qt::Key_W, Qt::NoModifier);
            view->sendEvent(view->rootObject()->childAt(x,y),&keyPressEvent_W);
            QKeyEvent keyReleaseEvent_W(QEvent::KeyRelease, Qt::Key_W, Qt::NoModifier);
            view->sendEvent(view->rootObject()->childAt(x,y),&keyReleaseEvent_W);

            qDebug()<<keyName;

        }

       if(keyName=="32")// 32 - "SPACE"
       {
           QKeyEvent keyPressEvent_Space(QEvent::KeyPress, Qt::Key_Space, Qt::NoModifier);
           view->sendEvent(view->rootObject()->childAt(x,y),&keyPressEvent_Space);
           QKeyEvent keyReleaseEvent_Space(QEvent::KeyRelease, Qt::Key_Space, Qt::NoModifier);
           view->sendEvent(view->rootObject()->childAt(x,y),&keyReleaseEvent_Space);
       }


}

void QuickEvent::cursorPosition(float y, float x)
{
    QMouseEvent pressEvent(QEvent::MouseButtonPress, QPoint(x, y),
                           Qt::NoButton, Qt::NoButton, Qt::NoModifier);
    QApplication::sendEvent(view->rootObject()->childAt(x,y), &pressEvent);



    QMouseEvent releaseEvent(QEvent::MouseButtonRelease, QPoint(x, y),
                           Qt::NoButton, Qt::NoButton, Qt::NoModifier);
            QApplication::sendEvent(view->rootObject()->childAt(x,y), &releaseEvent);






//                QMouseEvent moveEvent1(QEvent::MouseTrackingChange, QPoint(x, y),
//                                       Qt::NoButton, Qt::NoButton, Qt::NoModifier);
//                QApplication::sendEvent(view->rootObject()->childAt(x,y), &moveEvent1);


//                QMouseEvent moveEvent(QEvent::MouseMove, QPoint(x, y),
//                                       Qt::NoButton, Qt::NoButton, Qt::NoModifier);
//                QApplication::sendEvent(view->rootObject()->childAt(x,y), &moveEvent);

//            const bool isSent = /*view->sendEvent(view->rootObject()->childAt(x,y),&pressEventt);*/
//                    QApplication::sendEvent(view->rootObject()->childAt(x,y), &releaseEvent);
//            qDebug() << "'Press' at (" << x << "," << y << ") successful? " << isSent;
}

void QuickEvent::mouseDirectionEmit_(QString directName,float x,float y)
{

    if(directName=="Up")
    {
        cursorPosition(y, x);
    }

    if(directName=="Bottom")
    {
        cursorPosition(y, x);
    }

    if(directName=="Left")
    {
        cursorPosition(y, x);
    }

    if(directName=="Right")
    {
        cursorPosition(y, x);
    }

    if(directName=="Up_Right")
    {
        cursorPosition(y, x);
    }

    if(directName=="Right_Bottom")
    {
        cursorPosition(y, x);
    }

    if(directName=="Bottom  _Left")
    {
        cursorPosition(y, x);
    }

    if(directName=="Left_Up")
    {
        cursorPosition(y, x);
    }
    if(directName=="Center")
    {
        cursorPosition(y, x);
    }


}

