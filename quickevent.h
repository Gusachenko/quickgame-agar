#ifndef QUICKTEST_H
#define QUICKTEST_H

#include <QQuickItem>

class QuickEvent : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(NOTIFY nameChanged)
public:
    QuickEvent();

    void showQuick();
    void cursorPosition(float y, float x);
private:
    void mousePressEvent(QMouseEvent *event);

signals:
     void nameChanged();
public slots:
     void keyEmit_(QString keyName);
     void mouseDirectionEmit_(QString directName,float x,float y);
};

#endif // QUICKTEST_H
