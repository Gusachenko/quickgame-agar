#include "scene_area.h"
#include "quickevent.h"

#include <QApplication>


#include <QQmlEngine>
#include <QQmlContext>
#include <QQmlComponent>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<Scene_Area>("ModuleName", 1, 0, "TypeName");
    qmlRegisterType<QuickEvent>("KeyEmiterModule",1,0,"KeyEmiterType");


    QuickEvent * quickT=new QuickEvent();
    quickT->showQuick();


    return app.exec();


}
