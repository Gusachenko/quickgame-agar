#ifndef SCENE_AREA_H
#define SCENE_AREA_H

#include <QObject>

class Scene_Area : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString someText_ READ getSomeText_ WRITE setSomeText_ NOTIFY nameChanged)

public:
    explicit Scene_Area(QObject *parent = 0);


    QString getSomeText_()const;
    void setSomeText_(const QString &);


private:
    QString someText_;


    QString m_someText_;

signals:
    void someSignal_();
    void nameChanged();
public slots:
    void mySlot();

};

#endif // SCENE_AREA_H
